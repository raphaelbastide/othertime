var d = document
var lines = d.querySelectorAll('.lines p')
var currentLine = 0


function start(){
  run()
}
function run(t){
  setInterval(function(){
    for (var i = 0; i < lines.length; i++) {
      lines[i].classList.remove('active')
    }
    lines[currentLine].classList.add('active')
    if (currentLine +1 >= lines.length) {
      currentLine = 0
    }else {
      currentLine++
    }
  },12000)
}

start()

//start/stop button
var stopBtn = document.getElementById('stop')
var b = d.body
var playing = false

var isFirefox = typeof InstallTrigger !== 'undefined';
if (!isFirefox) {
  stopBtn.setAttribute('style','display:block;')
  stopBtn.addEventListener('click', () => togglePlay())
}else {
  stopBtn.setAttribute('style','display:none;')
}

function togglePlay(){
  if (playing) {
    Tone.Transport.stop()
    playing = false
    stopBtn.innerHTML = ">"
  }else {
    d.getElementById('player').play()
    playing = true
    stopBtn.innerHTML = ""
  }
}
