var d = document
var el = d.querySelectorAll('.input')
var v = d.querySelector('video')
var cleanbtn = d.querySelector('.cleanbtn')
var animstate = false
var btninplace = false

cleanbtn.onclick = function(){
  animstate = false
  if (!btninplace) {
    placebtn()
  }
  v.play()
  btninplace = false
}

v.onplay = function(){
  this.controls = false
}
v.onended = function(){
  animstate = false
}

v.addEventListener('timeupdate',function() {
  if(this.currentTime >= .81){
    if (!animstate) {
      anim()
    }
    animstate = true
  }
})


function anim(){
  d.body.classList.add('play')
  for (var i = 0; i < el.length; i++) {
    var rleft2 = 500 + Math.floor((Math.random() * 5) + 1);
    el[i].style.animationName = 'i'+i
    d.getElementById('style').innerHTML += "@keyframes i"+i+" {0%{margin-left: 0;}80%{margin-left:"+rleft2+"px;}100%{margin-left: "+rleft2+"px;}}"
  }
}


function placebtn(){
  d.body.classList.remove('play')
  d.getElementById('vid').load()
  d.getElementById('style').innerHTML = ""
  for (var i = 0; i < el.length; i++) {
    var rtop = Math.floor((Math.random() * (549 - 309)) + 309);
    var rleft = Math.floor((Math.random() * (369 - 279)) + 279);
    var rdur = (Math.random() * .5) + .3;

    el[i].setAttribute('style',
    'top:'+rtop+'px;left:'+rleft+'px; animation-duration:'+rdur+'s;')
    el[i].style.animationName = 'e'
    el[i].style.top = rtop+'px'
    el[i].style.left = rleft+'px'
    el[i].style.animationDuration = rdur+'s'
  }
  btninplace = true
}
placebtn()
