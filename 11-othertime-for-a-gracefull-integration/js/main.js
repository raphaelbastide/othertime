// import * as THREE from '../build/three.module.js';
var camera, scene, renderer;
var isUserInteracting = false,
  onMouseDownMouseX = 0, onMouseDownMouseY = 0,
  lon = 0, onMouseDownLon = 0,
  lat = 0, onMouseDownLat = 0,
  phi = 0, theta = 0;

  var g = new THREE.BoxGeometry( 1, 1, 1 );
  g.scale( 100, 20, 10 );
  var m = new THREE.MeshBasicMaterial( { color: 0xFF0000 } );
  var group = new THREE.Group();

init();
animate();
function init() {
  var container, mesh;
  container = document.getElementById( 'container' );
  camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1100 );
  camera.target = new THREE.Vector3( 0, 0, 0 );
  scene = new THREE.Scene();
  var geometry = new THREE.SphereBufferGeometry( 500, 60, 40 );
  // invert the geometry on the x-axis so that all of the faces point inward
  geometry.scale( - 1, 1, 1 );
  var texture = new THREE.TextureLoader().load( 'beaux-arts.jpg' );
  var material = new THREE.MeshBasicMaterial( { map: texture } );
  mesh = new THREE.Mesh( geometry, material );
  scene.add( mesh );

  var cube = new THREE.Mesh( g, m );

  // cube.position.x = -120
  // cube.position.y = 200
  // cube.position.z = 420
  // cube.rotation.z = -.02
  // scene.add( cube );
  group.add( cube );



	var loader = new THREE.FontLoader();
	loader.load( 'chicago.json', function ( font ) {
		var textGeo = new THREE.TextBufferGeometry( "  COMPUTERS ", {
			font: font,
			size: 10,
			height: .1,
			curveSegments: 2,
			bevelThickness: 1,
			bevelSize: .1,
			bevelEnabled: false
		} );
		textGeo.computeBoundingBox();
		var textMaterial = new THREE.MeshBasicMaterial( { color: 0xFFFFFF});
		var mesh = new THREE.Mesh( textGeo, textMaterial );
		mesh.position.x = 50;
    mesh.position.y =  3;
    mesh.position.z =  8;
    mesh.rotation.z = 3.15
		mesh.castShadow = true;
		mesh.receiveShadow = true;
		// scene.add( mesh );
    group.add( mesh );
	} );

group.position.x = -120
group.position.y = 200
group.position.z = 420
group.rotation.z = -.02

  scene.add( group );


  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  container.appendChild( renderer.domElement );
  document.addEventListener( 'mousedown', onPointerStart, false );
  document.addEventListener( 'mousemove', onPointerMove, false );
  document.addEventListener( 'mouseup', onPointerUp, false );
  document.addEventListener( 'wheel', onDocumentMouseWheel, false );
  document.addEventListener( 'touchstart', onPointerStart, false );
  document.addEventListener( 'touchmove', onPointerMove, false );
  document.addEventListener( 'touchend', onPointerUp, false );
  //
  document.addEventListener( 'dragover', function ( event ) {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'copy';
  }, false );
  document.addEventListener( 'dragenter', function () {
    document.body.style.opacity = 0.5;
  }, false );
  document.addEventListener( 'dragleave', function () {
    document.body.style.opacity = 1;
  }, false );
  document.addEventListener( 'drop', function ( event ) {
    event.preventDefault();
    var reader = new FileReader();
    reader.addEventListener( 'load', function ( event ) {
      material.map.image.src = event.target.result;
      material.map.needsUpdate = true;
    }, false );
    reader.readAsDataURL( event.dataTransfer.files[ 0 ] );
    document.body.style.opacity = 1;
  }, false );
  //
  window.addEventListener( 'resize', onWindowResize, false );
}


function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
}
function onPointerStart( event ) {
  isUserInteracting = true;
  var clientX = event.clientX || event.touches[ 0 ].clientX;
  var clientY = event.clientY || event.touches[ 0 ].clientY;
  onMouseDownMouseX = clientX;
  onMouseDownMouseY = clientY;
  onMouseDownLon = lon;
  onMouseDownLat = lat;
}
function onPointerMove( event ) {
  if ( isUserInteracting === true ) {
    var clientX = event.clientX || event.touches[ 0 ].clientX;
    var clientY = event.clientY || event.touches[ 0 ].clientY;
    lon = ( onMouseDownMouseX - clientX ) * 0.1 + onMouseDownLon;
    lat = ( clientY - onMouseDownMouseY ) * 0.1 + onMouseDownLat;
  }
}
function onPointerUp() {
  isUserInteracting = false;
}
function onDocumentMouseWheel( event ) {
  var fov = camera.fov + event.deltaY * 0.5;
  camera.fov = THREE.Math.clamp( fov, 10, 75 );
  camera.updateProjectionMatrix();
}
function animate() {
  requestAnimationFrame( animate );
  update();
}
function update() {
  if ( isUserInteracting === false ) {
    // lon += 0.1;
  }
  group.rotation.x += .05;
  lat = Math.max( - 85, Math.min( 85, lat ) );
  phi = THREE.Math.degToRad( 90 - lat );
  theta = THREE.Math.degToRad( lon );
  camera.target.x = 500 * Math.sin( phi ) * Math.cos( theta );
  camera.target.y = 500 * Math.cos( phi );
  camera.target.z = 500 * Math.sin( phi ) * Math.sin( theta );
  camera.lookAt( camera.target );
  /*
  // distortion
  camera.position.copy( camera.target ).negate();
  */
  renderer.render( scene, camera );
}
