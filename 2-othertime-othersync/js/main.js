var img = document.getElementById('img')
var body = document.body
var canvas = document.getElementById('canvas')

img.onmousemove = function(e) {
  canvas.width = this.width;
  canvas.height = this.height;
  canvas.getContext('2d').drawImage(this, 0, 0, this.width, this.height);
  var pixelData = canvas.getContext('2d').getImageData(e.offsetX, e.offsetY, 1, 1).data;
  console.log(pixelData);
  img.setAttribute('style','background-color:rgb('+pixelData[0]+','+pixelData[1]+','+pixelData[2]+');')
};
