// var piece = "piece.json";
var autoDelaySpeed = .10,
    body = $('body'),
    content = $('.message-box'),
    time = {f0 : 0, f1 : 0, f2 : 0};

$.getJSON(piece, function(data) {
  // $('.title').append(data.title);
  // $('.author').append(data.author);
  // $('.title').append(data.title);
  var frame = lastFrame = lastFrameTime = false;
  var lines = data.action,
      lastDelay, typeDelay

  // Environement opptions (loop, production mode, speed)
  if (typeof data.loop !== 'undefined' && data.loop == true) {
    var loop = true;
    var loopCycle = 0;
    body.addClass('looped');
  }else {
    loopCycle = 0;
  }
  if (typeof data.prod !== 'undefined' && data.prod == true) {
    var prod = true;
    body.addClass('prod');
  }else {
    var prod = false;
  }
  if (typeof data.speed !== 'undefined') {
    var speed = data.speed;
  }else {
    var speed = 1;
  }

  body.addClass(slug(data.title, {lower:true}));
  console.log("—   —  — –––——————————— –  –   –");
  console.log('Starting:'+data.title+' with speed='+speed);

  //     Frames model
  //     [f0[f1][f2]]

  function lineParser(loopCycle){
  // Loop through the lines
  time = {f0 : 0, f1 : 0, f2 : 0};

    // lastmaintime = 0;
    for (var i = 0; i < lines.length; ++i){
      // List all the messenger lines
      writeLines(i, loopCycle);
      // Update the times respectively
      getTime(i);
      if (frame != lastFrame && frame != "f0") {
        // If new frame that is not main
        if (lastFrame != "f0") {
          // new frame, not main, last isn’t main
          time[frame] = lastMainTime + delay;
        }else {
          // new frame, not main, last is main
          time[frame] = lastMainTime + delay;
          console.log('lastmaintime:'+lastMainTime);
        }
        if (frame != "f0" && lastFrame == "f0") {
          time[frame] = time['f0'] + delay;
        }
      }else if(lastFrame == false && frame == "f0"){
        // If main frame and first frame
      }
      if(frame == "f0"){
        lastMainTime = time['f0']
        time[frame] = time['f0'] + delay;
        console.log("main, lastmaintime ="+lastMainTime);
      }else {
        time[frame] += delay;
      }
      // time[frame] += delay;
      // time.f0 += delay;
      lastFrame = frame;
      console.log(time);
      // Let’s start all the timers, line by line
      revealLine(i, loopCycle, time[frame], lastMainTime, delay, typeDelay, lines.length, frame);
      // We add the type delay to the time
      time[frame] += typeDelay;
      time.f0 += typeDelay;
    }
  }
  lineParser(loopCycle);

  // Write the given lines
  function writeLines(id, loopCycle) {
    charname = lines[id].char;
    charslug = slug(charname, {lower:true});
    text = lines[id].text;
    // Replace a character
    text = text.replace(/\○/g, '<b>●</b>');
    // Set the line’s frame in case of split function
    // f0 is the main frame
    if (typeof lines[id].frame != "undefined") {
      frame = "f"+lines[id].frame;
    }else {
      frame = "f0";
    }
    id = id+'l'+loopCycle;
    logline = "<span class='charpp'><span>"+charname+"</span></span> <span class='bubble'><span class='typing ellipsis'><i>●</i><i>●</i><i>●</i></span><span class='text'>"+text+"</span></span>";
    // Insert Logline in Line box
    $("<div id='id-"+id+"' class='linebox char-"+charslug+" "+frame+"'><div class='line'>"+logline+"</div>").insertBefore(content.children("footer"));
    console.log('A line listed');
  }


  function revealLine(id, loopCycle, time, lastMainTime, delay, typeDelay, totalLines, frame){
    // Display the line boxes one after the other
    timer = setTimeout(function() {
      var currentLineBox = $('#id-'+id+'l'+loopCycle);
      currentLineBox.addClass('shown');
      // Type delay timer
      setTimeout(function(){
        currentLineBox.addClass('showtext');
        // In script functions launcher
        if (typeof lines[id].func != "undefined") {
          func = lines[id].func;
          console.log("Executing function: "+func);
          if (typeof lines[id].funcParams != "undefined") {
            funcParams = lines[id].funcParams;
            exeFunc(func, id, funcParams)
          }else {
            exeFunc(func, id)
          }
        }
        // Test if this is the end
        if (id === totalLines - 1) {
          if (loop) {
            loopCycle ++;
            lineParser(loopCycle);
          }else {
            stoper();
          }
        }
      }, typeDelay * 1000 * (1 / speed));
      clearOldLines();
      content.animate({scrollTop:$(document).height()}, '0');
      console.log("id="+id+", time."+frame+"="+time+", lastMainTime="+lastMainTime+", delay="+delay+", typeDelay="+typeDelay+", speed="+speed) ;
    }, time * 1000 * (1 / speed));
  }

  function getTime(id){
    text = lines[id].text;
    autoTypeDelay = Math.round(text.length * autoDelaySpeed);
    if (typeof lines[id].delay != "undefined") {
      // Set delay
      delay = lines[id].delay;
    }else {
      // Has no delay
      delay = 1;
    }
    if (typeof lines[id].typedelay != "undefined") {
      // has type delay
      typeDelay = lines[id].typedelay;
    }else {
      // Has no type delay
      typeDelay = autoTypeDelay ;
    }

    // If framed
    if (typeof lines[id].frame != "undefined") {
      frame = "f"+lines[id].frame;
    }else {
      frame = "f0";
    }
  }
})

// In Pieces function constructor
function exeFunc(funcName, id, funcParams){
  var fn = window[funcName];
  funcParams.unshift(id)
  // console.log(funcParams);
  if (typeof fn === "function") fn.apply(null, funcParams);
}

// Basic clock for debugging
var t = 0;
console.log("🕓 "+ t++);
timerId = setTimeout(function tick() {
  console.log("🕓 "+ t++);
  timerId = setTimeout(tick, 1000);
}, 1000);

// Function ran when the pièce is over
function stoper() {
  clearTimeout(timer);
  clearTimeout(timerId);
  console.log("—   —  — –––——————————— –  –   –");
  console.log('Timers stopped, end of the pièce');
  console.log("—   —  — ––– Bravo! ——— –  –   –");
  $('.end').addClass('visible');
}

$(document).keypress(function( event ) {
  if ( event.which == 13 || event.which == 32 ) {
    body.toggleClass('prod');
  }
  if ( event.which == 112) {
    stoper();
  }
});


// Clear Offscreen
jQuery.expr.filters.offscreen = function(el) {
  var rect = el.getBoundingClientRect();
  return (
  (rect.x + rect.width) < 0
     || (rect.y + rect.height) < 0
     || (rect.x > window.innerWidth || rect.y > window.innerHeight)
  );
};
function clearOldLines(){
  var lines = $('.linebox');
  lines.each(function(){
    if (!$(this).visible(true)) {
      $(this).remove();
    }
  })
}
