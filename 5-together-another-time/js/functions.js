
// Custom Functions used in pieces



// Split func
function split(parts, toggle){
  if (toggle) {
    setTimeout(function(){
      $(".message-box").addClass('split');
    }, 0);
  }else {
    $(".message-box").removeClass('split');
  }
}

// Test func
function test(state, string){
  if (state) {
    console.log(string);
  }else {
    console.log("test = false");
  }
}

// AddClass func
function lineClass(line, cl){
  var line = $('#id-'+line);
  console.log(line, cl);
  setTimeout(function(){
    line.addClass(cl);
  }, 1000);
}

// mainClass func
function mainClass(line, cl){
  setTimeout(function(){
    $(".message-box").addClass(cl);
  }, 100);
}
