function start(){
  var p = plist[Math.round(Math.random()* plist.length)]
  var string = document.getElementById('string')
  string.innerHTML = "";
  console.log(p);

  var charray = p.split("");
  function isOdd(num) { return num % 2;}

  var msynth = new Tone.MembraneSynth({

    volume:-15,
    pitchDecay : .2 ,
    octaves : 1.5 ,
    oscillator : {
      // type : "sawtooth20"
      type : "sine"
    },
    envelope : {
      attack : 0.3 ,
      decay : 0.1 ,
      sustain : 0.1 ,
      release : .4
      }
    }
  ).toMaster();
  var ding =  new Tone.PluckSynth({
    attackNoise : 1 ,
    dampening : charray.length * 1000 ,
    resonance : .89
  }).toMaster();

  var tink = new Tone.MetalSynth( {
    volume:.2,
    frequency : charray.length * 80 ,
    envelope : {
      attack : 0.1 ,
      decay : .1,
      release : charray.length * .6
    },
    harmonicity : 1.5 ,
    modulationIndex : 1,
    resonance : charray.length * 190,
    octaves : 3
  }

  ).toMaster();

  var synth = new Tone.Synth().toMaster();

  var f = charray[0];
  var l = charray.length
  var dingInterval =  Math.round(l / 3)


    var i = 0, n, b = 10, a = 20
    var interval = setInterval(function() {
      if(i >= charray.length){
        console.log('end');
        clearInterval(interval);
        setTimeout(function(){start()},7000)
      }else {
        var c = charray[i]
        var u = c.charCodeAt(0)
        n = (150 - u) *10;

        if (i % dingInterval == 0 && i != 0) {
          if (isOdd(charray.length)) {
            setTimeout(function(){
              tink.triggerAttackRelease(i*100, "7n");

            }, 100)

          }else {
            ding.triggerAttackRelease(i*100, "1n");
            if (isNaN(charray[0])) {
              setTimeout(function(){
                ding.triggerAttackRelease(i*100, "1n");
              }, 100)
            }
          }
          msynth.triggerAttackRelease(n, "4n");
        }else {
          msynth.triggerAttackRelease(n, "4n");
        }
        string.innerHTML += c;
        i++;
      }
    }, 200 * l / 3 );


}
start()


var stopBtn = document.getElementById('stop')

var isFirefox = typeof InstallTrigger !== 'undefined';
if (!isFirefox) {
  var playing = false
  stopBtn.setAttribute('style','display:block;')
  stopBtn.addEventListener('click', () => togglePlay())
}else {
  stopBtn.setAttribute('style','display:none;')
}

function togglePlay(){
  if (playing) {
    Tone.Transport.stop()
    playing = false
    stopBtn.innerHTML = ">"
  }else {
    stopBtn.innerHTML = ""
  }
}
