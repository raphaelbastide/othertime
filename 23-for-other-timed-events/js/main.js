
var soundClips = document.querySelector('.sound-clips');
var canvas = document.querySelector('.visualizer');
var mainSection = document.querySelector('.main-controls');
var canvasCtx = canvas.getContext("2d");
var start = document.getElementById('start')
var p = document.getElementById('stop')
var t = document.getElementById('time')
var play = false
var si
// p.addEventListener('change', e => Tone.Transport.toggle())

 var k1 = new Tone.MembraneSynth({
   "volume" : .9,
   "envelope" : {
     "sustain" : 0,
     "attack" : 0.002,
     "decay" : 0.9
   },
   "octaves" : 3
 }).toMaster();

 var k1Part = new Tone.Loop(function(time){
   k1.triggerAttackRelease("a1", "2n", time);
 }, "1.2").start("0");

var ch = new Tone.NoiseSynth( {
noise : {
  type : 'brown'
},
envelope : {
  attack : 0.05 ,
  decay : 0.1 ,
  sustain : 0.2
  }
}
).toMaster();


var chpart = new Tone.Loop(function(time){
  ch.triggerAttackRelease("25n");
}, "1.2").start(".8");
var chpart2 = new Tone.Loop(function(time){
  ch.triggerAttackRelease("10n");
}, "1.2").start(".3");
  chpart.probability =  .70
  chpart2.probability = .70

function loop(audio){
  play = true
  var steps = []
  for (var i = 0; i < 5; i++) {
    var r = Math.round(Math.random()*5)
    steps.push(r)
  }
  document.body.setAttribute('class','')
  document.body.classList.add('playing')
  Tone.Transport.start()
  var t = 300
  si = setInterval(function(){
    audio.currentTime = steps[0];
    audio.play()
    setTimeout(function(){
      audio.currentTime = steps[1];
      audio.play()
    }, t*2)
    setTimeout(function(){
      audio.currentTime = steps[2];
      audio.play()
    }, t*4)
    setTimeout(function(){
      audio.currentTime = steps[3];
      audio.play()
    }, t*6)
    setTimeout(function(){
      audio.currentTime = steps[4];
      audio.play()
    }, t*7)
  },t*8)
}
document.body.addEventListener('click', () => run())
function run(){

  // var audioCtx = new (window.AudioContext || webkitAudioContext)();

  if (navigator.mediaDevices.getUserMedia) {
    console.log('getUserMedia supported.');
    var constraints = { audio: true };
    var chunks = [];
    var onSuccess = function(stream) {
      var mediaRecorder = new MediaRecorder(stream);
      visualize(stream);
      start.onclick = function(){
        record()
        setTimeout(function(){
          stop()
        },5000)
      }
      p.addEventListener('click', () => togglePlay())
      function togglePlay(){
        console.log('test');
        if (Tone.context.state !== 'running') {
          Tone.context.resume();
      }

        if (play) {
          Tone.Transport.stop()
          play = false
          stop()
        }else {
          Tone.Transport.start()
          play = true
          record()
        }
      }
      function record() {
        if (play) {
          stop()
        }
        mediaRecorder.start();
        console.log(mediaRecorder.state);
        console.log("recorder started");
        document.body.setAttribute('class','')
        document.body.classList.add('recording')
      }

      function stop() {
        document.body.setAttribute('class','')
        mediaRecorder.stop();
        console.log(mediaRecorder.state);
        console.log("recorder stopped");
        document.getElementById('player').src =""
        soundClips.innerHTML = "";
        clearInterval(si);
        play = false
      }

      mediaRecorder.onstop = function(e) {
        console.log("data available after MediaRecorder.stop() called.");
        var audio = document.createElement('audio');
        audio.setAttribute('controls', '');
        soundClips.appendChild(audio);
        audio.id = 'player'
        audio.controls = true;
        var blob = new Blob(chunks, { 'type' : 'audio/ogg; codecs=opus' });
        chunks = [];
        var audioURL = window.URL.createObjectURL(blob);
        audio.src = audioURL;
        console.log("recorder stopped");
        loop(audio)
      }

      mediaRecorder.ondataavailable = function(e) {
        chunks.push(e.data);
      }
    }

    var onError = function(err) {
      console.log('The following error occured: ' + err);
    }

    navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onError);

  } else {
     console.log('getUserMedia not supported on your browser!');
  }
}

function visualize(stream) {
  var audioCtx = new (window.AudioContext || webkitAudioContext)();
  var source = audioCtx.createMediaStreamSource(stream);
  var analyser = audioCtx.createAnalyser();
  analyser.fftSize = 2048;
  var bufferLength = analyser.frequencyBinCount;
  var dataArray = new Uint8Array(bufferLength);
  source.connect(analyser);
  draw()
  function draw() {
    WIDTH = canvas.width
    HEIGHT = canvas.height;
    requestAnimationFrame(draw);
    analyser.getByteTimeDomainData(dataArray);
    canvasCtx.fillStyle = 'rgb(200, 200, 200)';
    canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
    canvasCtx.lineWidth = 2;
    canvasCtx.strokeStyle = 'rgb(0, 0, 0)';
    canvasCtx.beginPath();
    var slicedHeight = HEIGHT * 1.0 / bufferLength;
    var y = 0;
    for(var i = 0; i < bufferLength; i++) {
      var v = dataArray[i] / 128.0;
      var x = v * WIDTH/2;
      if(i === 0) {
        canvasCtx.moveTo(x, y);
      } else {
        canvasCtx.lineTo(x, y);
      }
      y += slicedHeight;
    }
    canvasCtx.lineTo(canvas.height/2, canvas.widtth);
    canvasCtx.stroke();
  }
}
window.onresize = function() {
  canvas.height = mainSection.offsetWidth;
}
window.onresize();
