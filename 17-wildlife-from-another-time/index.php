<!DOCTYPE html>
<!--
PART OF otherti.me
BY RAPHAĖL BASTIDE
INITIATED IN NOVEMBER 2019
-->
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>othertime</title>
    <meta name="description" content="A web based artwork a day" />
    <link rel="icon" type="image/png" href="https://otherti.me/home/img/favicon.png">
    <meta property="og:title" content="othertime" />
    <meta property="og:image" content="https://otherti.me/home/img/og.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="otherti.me" />
    <meta name="twitter:image" content="https://otherti.me/home/img/favicon.png" />
    <meta name="twitter:description" content="A web based artwork a day" />
    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    </head>
    <body>
      <main>
        <canvas id="tail" resize></canvas>
        <canvas id="body" resize></canvas>
        <canvas id="head" resize></canvas>
      </main>
      <section class="form-mess">
        <button id="reset">RESET</button>
        <!-- <form class="" action="index.php" method="post"> -->
        <div >
          <input id="svgInput" hidden type="text" name="svg" value="">
          <input id="partInput" hidden type="text" name="part" value="">
          <button id="submit" >SEND (inactive)</button>
        </div>
      </section>

      <section class="collage">
        <p class="addmore">MORE DRAWING NEEDED</p>
        <?php
        $hasSVG = false;
        $filesNbr = [];
        $currentNbr = 0;
        $files = scandir('svgs');

        foreach ($files as $file) {
          $ext = pathinfo($file, PATHINFO_EXTENSION);
          if ($ext == "svg") {
            $hasSVG = true;
            $currentNbr = get_string_between($file,"-",".");
            array_push($filesNbr, $currentNbr);
          }
        }

        if (isset($_POST['part'])) { // if form sent
          if (!$hasSVG) {
            $filesNbr = [0];
          }
          $nextNbr = max($filesNbr) + 1;
          $part = $_POST['part'];
          $path = 'svgs/'.$part.'-'.$nextNbr.'.svg';
          $file = fopen($path, "w");
          $current = $_POST['svg'];
          file_put_contents($path, $current);
          $hasSVG = true;
          $files = scandir('svgs');
        }

        foreach ($files as $file) {
          $ext = pathinfo($file, PATHINFO_EXTENSION);
          $part = substr($file, 0, 4);
          if ($ext == "svg") {
            echo "<img class='part-image ".$part."' src='svgs/".$file."'>";
          }
        }

        function get_string_between ($str,$from,$to) {
          $string = substr($str, strpos($str, $from) + strlen($from));
          if (strstr ($string,$to,TRUE) != FALSE) {
            $string = strstr ($string,$to,TRUE);
          }
          return $string;
        }
        ?>
      </section>
      <footer>All drawings posted here are under <a href="https://creativecommons.org/publicdomain/zero/1.0/">CC0</a> (public domain)</footer>
    </body>
  </body>
  <script type="text/javascript" src="https://otherti.me/othertime.js"></script>
  <script type="text/javascript" src="js/paper.js"></script>
  <script id="script" type="text/paperscript" canvas="" src="js/main.js"></script>
  <script type="text/javascript">
    var parts = ['tail','body','head']
    var r = Math.round(Math.random() * 2)
    var chosenPart = parts[r]
    console.log(chosenPart);
    document.body.classList.add(chosenPart)
    document.getElementById('script').setAttribute('canvas',chosenPart)
    document.getElementById('partInput').setAttribute('value',chosenPart)
  </script>
</html>
