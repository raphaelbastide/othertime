
var path

var resetBtn = document.getElementById('reset')
var input = document.getElementById('svgInput')

var submitVisible = false
window.app = {
  curveless: new Tool({
    onMouseDown: function(event){
      path = new Path();
      path.strokeColor = 'black';
      path.strokeWidth = '2'
      path.add(event.point);
    },
    onMouseDrag: function(event) {
      path.lineTo(event.point);
      var submit = document.getElementById('submit')
      if (!submitVisible) {
        submit.classList.add('visible')
        submitVisible = true
      }
    },
    onMouseUp: function(event){
      var svg = project.exportSVG({ asString: true });
      svgInput.value = svg
    }
  }),
};

resetBtn.addEventListener('click', function(){
  project.clear()
})

var h = document.querySelectorAll('.part-image.head')
var b = document.querySelectorAll('.part-image.body')
var t = document.querySelectorAll('.part-image.tail')
pick()
setInterval(function(){
  pick()
}, 3000);
document.addEventListener('keydown', checkKey);

function checkKey(e) {
  if (e.code == "Space") {
    pick()
  }
}

function pick(){
  if (h.length > 0 && b.length > 0 && t.length > 0) {
    var img = document.querySelectorAll('.part-image.selected')
    var rh = Math.ceil(Math.random() * h.length - 1 )
    var rb = Math.ceil(Math.random() * b.length - 1 )
    var rt = Math.ceil(Math.random() * t.length - 1 )
    console.log(rh);
    for (var i = 0; i < img.length; i++) {
      img[i].classList.remove('selected')
    }
    h[rh].classList.add('selected')
    b[rb].classList.add('selected')
    t[rt].classList.add('selected')
  }else {
    document.body.classList.add('add')
  }

}
