
        window.requestAnimFrame =
            window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function(callback) {
                return window.setTimeout(callback, 1000 / 60);
        };

        var canvas = document.getElementById('canvas'),
            ctx    = canvas.getContext('2d');

        canvas.width = 500;
        canvas.height = 500;

        /* Customisable map data */
        var map = {

            level:0,
            tile_size: 20,

            /*

            Key vairables:

            id       [required] - an integer that corresponds with a tile in the data array.
            colour   [required] - any javascript compatible colour variable.
            solid    [optional] - whether the tile is solid or not, defaults to false.
            bounce   [optional] - how much velocity is preserved upon hitting the tile, 0.5 is half.
            jump     [optional] - whether the player can jump while over the tile, defaults to false.
            friction [optional] - friction of the tile, must have X and Y values (e.g {x:0.5, y:0.5}).
            gravity  [optional] - gravity of the tile, must have X and Y values (e.g {x:0.5, y:0.5}).
            fore     [optional] - whether the tile is drawn in front of the player, defaults to false.
            script   [optional] - refers to a script in the scripts section, executed if it is touched.

            */

            keys: [
                {id: 0, colour: '#000', solid: 0},

                {id: 1, colour: '#fff', solid: 0},
                {id: 2,colour: '#000',solid: 1,bounce: 0.35},
                {id: 3,colour: '#C93232',solid: 0,script: 'death'},

                {id: 4,colour: 'CornflowerBlue',solid: 0,script: 'unlock1'},
                {id: 5,colour: 'DarkOliveGreen',solid: 0,script: 'unlock2'},
                {id: 6,colour: 'DeepPink',solid: 0,script: 'unlock3'},
                {id: 7,colour: 'Gold',solid: 0,script: 'unlock4'},
                {id: 8,colour: 'PaleVioletRed ',solid: 0,script: 'unlock5'},
                {id: 9,colour: 'OrangeRed',solid: 0,script: 'unlock6'},

                {id: 20,colour: '#fff',solid: 0,bounce: 0.35},
            ],

            /* An array representing the map tiles. Each number corresponds to a key */
            data: [

                [2, 2, 2 ,2, 2,20, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2],
                [2, 1, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2, 1,20, 1, 1, 1, 1, 1, 2],
                [2,20,20, 2, 1, 2, 2, 2,20, 1, 2, 1,20, 1, 2, 1, 2, 1, 2, 1, 2, 2, 2, 2, 2],
                [2, 1, 1, 2, 1, 2, 1, 1, 1, 1, 2, 1, 2, 1,20, 1, 2, 1, 2, 1,20, 1, 1, 1, 1],
                [2, 1, 4, 2, 1, 2, 1, 1, 1, 1,20, 1, 2, 2,20, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2],
                [2, 1, 1, 2, 1, 2, 2, 2, 2, 1,20, 1, 1, 1, 1, 1, 2, 1,20, 1, 1, 1, 1, 1, 2],
                [2, 1, 1, 2, 1,20, 1, 1, 1, 1,20, 1,20,20,20, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2],
                [2, 1, 2,20, 1,20, 1, 1, 1, 1, 2, 1,20, 7, 2, 1, 2, 1, 2, 1, 2, 1, 1, 1, 1],
                [2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 2, 1,20, 1, 2, 1, 2, 1, 2, 1, 2,20, 2, 2, 2],
                [2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1,20, 1, 2, 1, 2, 1, 2, 1, 1, 1, 1, 1, 2],
                [2, 1, 1, 2, 2, 2, 1, 1, 1, 1, 2, 2, 2, 1, 2,20, 2, 1, 2,20, 2, 2, 2, 2, 2],
                [2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2],
                [2, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2],
                [2, 2, 2, 1, 1, 1, 1, 2, 1, 1, 1, 1, 2, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 2],
                [2, 2, 2, 2, 2,20, 2, 2, 1, 2, 2,20, 1, 2, 2, 2, 2, 1, 2, 2, 2,20,20, 1, 2],
                [2, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1,20, 1, 1, 2, 1,20, 2,20, 1, 1, 1, 1 ,2, 2],
                [2, 1,20, 2, 2, 2, 2, 2, 1, 2, 1,20, 1, 1, 2, 8,20, 2, 2, 1, 2, 2, 1, 1, 2],
                [2, 1,20, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 2, 1,20,20, 2, 1, 2, 1,20 ,1, 2],
                [2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 2, 2, 1,20, 1, 2, 1, 2, 1,20, 1, 2],
                [2, 1, 1, 1, 1, 1, 1, 2, 1, 2, 1,20, 1, 1, 2, 1, 2, 1,20, 1, 2, 1, 2, 1, 2],
                [2, 1, 2,20, 2,20, 2, 2, 1, 2, 1, 2, 2, 1, 1, 1, 2, 1,20, 1,20, 1, 2, 1, 2],
                [2, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1,20, 1, 2, 1, 1, 2, 1, 2, 1,20, 1,20, 1, 2],
                [2, 1, 2,20, 2,20, 2, 2, 2, 2, 1, 2, 1, 1, 2, 1, 1, 1, 2, 1, 2, 2, 1, 9, 2],
                [2, 1, 1, 1, 1, 6, 1, 2, 2, 2, 1,20, 5, 1, 2, 1, 2, 2, 1, 1, 1, 1, 1, 2, 1],
                [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2 ,1, 1],

            ],

            /* Default gravity of the map */

            gravity: {
                x: 0,
                y: 0.3
            },

            /* Velocity limits */

            vel_limit: {
                x: 2,
                y: 16
            },

            /* Movement speed when the key is pressed */

            movement_speed: {
                jump: 6,
                left: 0.3,
                right: 0.3
            },

            /* The coordinates at which the player spawns and the colour of the player */

            player: {
                x: 4,
                y: 2,
                colour: 'gray'
            },

            /* scripts refered to by the "script" variable in the tile keys */
            scripts: {
                unlock1: 'this.current_map.keys[4].script = "";this.current_map.keys[4].colour = "#fff"; this.current_map.keys[2].colour = "CornflowerBlue"; this.current_map.level++; console.log(this.current_map.level); if(this.current_map.level >= 6){console.log("end"); var a = this.current_map.data; for (var i = 0; i < a.length; i++) {;for(var j = 0; j < a[i].length; j++){if(a[i][j].id == 20){this.current_map.keys[2].colour = "black"; a[i][j].colour = "black"; a[i][j].solid = 1; }}}};',
                unlock2: 'this.current_map.keys[5].script = "";this.current_map.keys[5].colour = "#fff"; this.current_map.keys[2].colour = "DarkOliveGreen"; this.current_map.level++; console.log(this.current_map.level); if(this.current_map.level >= 6){console.log("end"); var a = this.current_map.data; for (var i = 0; i < a.length; i++) {;for(var j = 0; j < a[i].length; j++){if(a[i][j].id == 20){this.current_map.keys[2].colour = "black"; a[i][j].colour = "black"; a[i][j].solid = 1; }}}};',
                unlock3: 'this.current_map.keys[6].script = "";this.current_map.keys[6].colour = "#fff"; this.current_map.keys[2].colour = "DeepPink";this.current_map.level++; console.log(this.current_map.level); if(this.current_map.level >= 6){console.log("end"); var a = this.current_map.data; for (var i = 0; i < a.length; i++) {;for(var j = 0; j < a[i].length; j++){if(a[i][j].id == 20){this.current_map.keys[2].colour = "black"; a[i][j].colour = " black"; a[i][j].solid = 1; }}}};',
                unlock4: 'this.current_map.keys[7].script = "";this.current_map.keys[7].colour = "#fff"; this.current_map.keys[2].colour = "Gold";this.current_map.level++; console.log(this.current_map.level); if(this.current_map.level >= 6){console.log("end"); var a = this.current_map.data; for (var i = 0; i < a.length; i++) {;for(var j = 0; j < a[i].length; j++){if(a[i][j].id == 20){this.current_map.keys[2].colour = "black"; a[i][j].colour = " black"; a[i][j].solid = 1; }}}};',
                unlock5: 'this.current_map.keys[8].script = "";this.current_map.keys[8].colour = "#fff"; this.current_map.keys[2].colour = "PaleVioletRed";this.current_map.level++; console.log(this.current_map.level); if(this.current_map.level >= 6){console.log("end"); var a = this.current_map.data; for (var i = 0; i < a.length; i++) {;for(var j = 0; j < a[i].length; j++){if(a[i][j].id == 20){this.current_map.keys[2].colour = "black"; a[i][j].colour = "black"; a[i][j].solid = 1; }}}};',
                unlock6: 'this.current_map.keys[9].script = "";this.current_map.keys[9].colour = "#fff"; this.current_map.keys[2].colour = "OrangeRed";this.current_map.level++; console.log(this.current_map.level); if(this.current_map.level >= 6){console.log("end"); var a = this.current_map.data; for (var i = 0; i < a.length; i++) {;for(var j = 0; j < a[i].length; j++){if(a[i][j].id == 20){this.current_map.keys[2].colour = "black"; a[i][j].colour = "black"; a[i][j].solid = 1; }}}};',
                death: 'document.body.classList.add("gameover");this.load_map(map);',
            }
        };

        /* Setup of the engine */

        var game = new Clarity();
        game.set_viewport(canvas.width, canvas.height);
        game.load_map(map);

        /* Limit the viewport to the confines of the map */

        game.limit_viewport = true;

        var Loop = function() {

            ctx.fillStyle = '#000';
            ctx.fillRect(0, 0, canvas.width, canvas.height);

            game.update();
            game.draw(ctx);


            window.requestAnimFrame(Loop);
        };

        /* Start */

        Loop();
