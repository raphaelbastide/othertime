(function(){
    //generate clock animations
    var now       = new Date(),
        hourDeg   = now.getHours() / 12   * 360 + now.getMinutes() / 60 * 30,
        minuteDeg = now.getMinutes() / 60 * 360 + now.getSeconds() / 60 * 6,
        secondDeg = now.getSeconds() / 60 * 360,
        stylesDeg = [
            "@keyframes rotate-hour{from{transform:rotate(" + (hourDeg - 180) + "deg);}to{transform:rotate(" + (hourDeg + 180) + "deg);}}",
            "@keyframes rotate-minute{from{transform:rotate(" + (minuteDeg - 180) + "deg);}to{transform:rotate(" + (minuteDeg + 180) + "deg);}}",
            "@keyframes rotate-second{from{transform:rotate(" + (secondDeg - 180) + "deg);}to{transform:rotate(" + (secondDeg + 180) + "deg);}}"
        ].join("");

    document.getElementById("style").innerHTML = stylesDeg;

})();

var imgs = ["1.gif", "2.png", "3.gif", "4.png", "5.gif", "6.gif", "7.png", "8.png", "9.png", "10.png", "11.gif", "12.png", "13.png", "14.png", "15.gif", "16.png", "17.png", "18.gif", "19.png", "20.gif", "21.gif", "22.gif", "23.png", "24.gif", "25.gif", "26.png", "27.gif", "28.png", "29.png", "30.gif", "31.png", "32.png", "33.gif", "34.gif", "35.png", "36.png", "37.gif", "38.png", "39.png", "40.gif", "41.gif", "42.png", "43.png", "44.gif", "45.gif", "46.png", "47.png", "48.gif", "49.png", "50.png", "51.gif", "52.gif", "53.gif", "54.png", "55.png", "56.png", "57.gif", "58.png", "59.png", "60.png", "61.png", "62.png", "63.gif", "64.gif", "65.png", "66.png", "67.png", "68.png",]

ih = pick(60)
im = pick(60)
is = pick(60)

document.getElementById("hour").innerHTML   = ih;
document.getElementById("minute").innerHTML = im;
document.getElementById("second").innerHTML = is;

function showTime(){
    var date = new Date();
    var h = date.getHours(); // 0 - 23
    var m = date.getMinutes(); // 0 - 59
    var s = date.getSeconds(); // 0 - 59
    var session = "AM";
    if(h == 0){
        h = 12;
    }
    if(h > 12){
        h = h - 12;
        session = "PM";
    }
    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;
    var time = h + ":" + m + ":" + s + " " + session;

    var hs = document.querySelectorAll('#second img')
    var hm = document.querySelectorAll('#minute img')
    var hh = document.querySelectorAll('#hour img')
    for (var i = 0; i < hs.length; i++) {
      if (i <= s) {
        hs[i].classList.add('visible')
      }
      else {
        hs[i].classList.remove('visible')
      }
    }
    for (var i = 0; i < hm.length; i++) {
      if (i <= m) {
        hm[i].classList.add('visible')
      }
      else {
        hm[i].classList.remove('visible')
      }
    }
    for (var i = 0; i < hh.length; i++) {
      if (i <= h) {
        hh[i].classList.add('visible')
      }
      else {
        hh[i].classList.remove('visible')
      }
    }

    setTimeout(showTime, 1000);
}
function pick(nbr){
  var imglist = ""
  for (var i = 0; i < nbr; i++) {
    var file = imgs[Math.floor(Math.random() * 68)]
    imglist += "<img src='img/"+file+"'>";
  }
  return imglist
}
showTime();
