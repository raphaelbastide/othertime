var carousel = document.querySelector('.carousel');
var cell = document.querySelectorAll('.cell');
var cellCount = cell.length - 1;
console.log(cellCount);
var selectedIndex = 0;

function rotateCarousel() {
  var angle = selectedIndex / cellCount * -360;
  carousel.style.transform = 'translateZ(-288px) rotateY(' + angle + 'deg)';
}

setInterval(function(){
  selectedIndex++;
  rotateCarousel();

}, 5000);
