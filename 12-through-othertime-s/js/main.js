
//start/stop the transport
var stopBtn = document.getElementById('stop')
var playing = true
// p.addEventListener('change', e => Tone.Transport.toggle())
stopBtn.addEventListener('click', () => togglePlay())

function togglePlay(){
  if (playing) {
    Tone.Transport.stop()
    playing = false
    stopBtn.innerHTML = ">"
  }else {
    Tone.Transport.start()
    playing = true
    stopBtn.innerHTML = "||"
  }
}

Tone.Transport.start()


document.body.onclick = function(){
  document.body.classList.add('grid')
  setTimeout(function(){
    document.body.classList.remove('grid')
  }, 2000);
};
