/**
 *
 * ╭─╮ ┬ ┬ ╭─╮ ╭─╮ ┬─╮   ╭┬╮ ┬ ╭╮╭ ╭─╮   ╭─╮ ╭─╮ ┬   ┬ ╭┬╮ ╭─╮ ╭─╮ ╭╮╭ ╭─╮
 * ╰─╮ │ │ ├─╯ ├┤  ├┬╯    │  │ │││ ├┤    ╰─╮ ├─╯ │   │  │  ├─╯ ├─┤ │││ ├┤
 * ╰─╯ ╰─╯ ┴   ╰─╯ ┴╰─    ┴  ┴ ╯╰╯ ╰─╯   ╰─╯ ┴   ┴─╯ ┴  ┴  ┴   ┴ ┴ ╯╰╯ ╰─╯
 * Under 2kb and without dependencies
 *
 * Bare minimum implementation of a Splitpane UI with supports for
 * horizontal and vertical layouts
 *
 * @copyright (c) 2016-2017, Web Semantics, Inc.
 * @author Adnan M.Sagar, PhD. <adnan@websemantics.ca>
 * @license Distributed under the terms of the MIT License.
 */


 // Begin additions Bastide
var note = 500
 var surfaces = []

   var k1 = new Tone.MembraneSynth({
     "volume" : -1,
     "envelope" : {
       "sustain" : 0,
       "attack" : 0.002,
       "decay" : 0.8
     },
     "octaves" : 2
   }).toMaster();
   var k1Part = new Tone.Loop(function(time){
     k1.triggerAttackRelease("C2", "8n", time);
   }, "1").start("2");

   var s2 = new Tone.NoiseSynth({
     "volume" : -8,
     "envelope" : {
       "attack" : 0.001,
       "decay" : 0.2,
       "sustain" : 0
     },
     "filterEnvelope" : {
       "attack" : 0.001,
       "decay" : 0.2,
       "sustain" : 0
     }
   }).toMaster();
   var s2Part = new Tone.Loop(function(time){
     s2.triggerAttack(time);
   }, "4").start("4");

   var s3 = new Tone.NoiseSynth({
     "volume" : -8,
     "envelope" : {
       "attack" : 0.1,
       "decay" : 0.9,
       "sustain" : .01
     },
     "filterEnvelope" : {
       "attack" : 0.01,
       "decay" : 0.1,
       "sustain" : .1
     }
   }).toMaster();
   var s3Part = new Tone.Loop(function(time){
     s3.triggerAttack(time);
   }, "4").start("1");

   var k4 = new Tone.MembraneSynth({
     "volume" : -5,
     "envelope" : {
       "sustain" : 0.001,
       "attack" : 0.02,
       "decay" : 0.2,
       "release" : .4
     },
     "octaves" : 1.05,
   }).toMaster();
   var k4Part = new Tone.Loop(function(time){
     k4.triggerAttackRelease("800", time);
   }, "3").start('3.2');
   var k4Part = new Tone.Loop(function(time){
     k4.triggerAttackRelease("700", time);
   }, "3").start('3.35');
   k4Part.probability = .95
   k4Part.humanize = true

  var k5 = new Tone.DuoSynth({
    "voice0" : {
     "volume" : -10 ,
     "portamento" : 0 ,

     "envelope" : {
     "attack" : 0.9 ,
     "decay" : 0.01 ,
     "sustain" : 0 ,
     "release" : 0.5
     }
   },

   "voice1" : {
     "volume" : -10 ,
     "portamento" : 0 ,

     "envelope" : {
       "attack" : 0.9 ,
       "decay" : 0.01 ,
       "sustain" : 0 ,
       "release" : 0.5
     }
   }
  }).toMaster();

  var bass = new Tone.Part(function(time, event){
		if (Math.random() < event.prob){
			k5.triggerAttackRelease(event.note, event.dur, time);
		}
	}, [
    { time : "0:0", note : "D3", dur : "1", prob : .9 },
    { time : "0:4", note : "B2", dur : "1", prob : .5 },
    { time : "0:8", note : "E2", dur : "1", prob : .9 },
  ]).start(0);

	bass.loop = true;
	bass.loopEnd = "8";
  // var k5Part = new Tone.Loop(function(time){
  //   k5.triggerAttackRelease(note, time);
  // }, "2").start(.05);

    var s6 = new Tone.NoiseSynth({
      "volume" : -19,
      "envelope" : {
        "attack" : 0.001,
        "decay" : 0.1,
        "sustain" : .01
      },
      "filterEnvelope" : {
        "attack" : 0.1,
        "decay" : 0.001,
        "sustain" : 0
      }
    }).toMaster();
    var s6Part = new Tone.Loop(function(time){
      s6.triggerAttack(time);
    }, ".2").start(0);
    s6Part.probability = .9
    s6Part.humanize = true


  	// s6Part.loop = true;
  	// s6Part.loopEnd = "7";

   //set the transport
   Tone.Transport.bpm.value = 90;
   // Tone.Reverb.decay = 1.5;

 var panes = document.querySelectorAll('.pane')
 var getSurfaces = debounce(function() {
   surfaces = [] //clear array
   for (var i = 0; i < panes.length; i++) {
     var w = panes[i].offsetWidth
     var h = panes[i].offsetHeight
     var s = w * h
     var p = panes[i].getAttribute('data-pane')
     surfaces.push(p,s)
   }
   var ta = (surfaces[1] /800000)
   var tb = (surfaces[3] /100000)
   var tc = (surfaces[5] /10000)
   var td = (surfaces[7] /10000)
   var te = (surfaces[9] /10000)
   var tf = (surfaces[11] /80000)
   k1Part.interval =  1 * ta
   s2Part.interval = 4 * tb
   s3Part.interval = 5 * tc
   k4Part.note = 3 * td
   s6Part.interval = .2 * tf
 }, 50);

 function debounce(func, wait, immediate) {
 	var timeout;
 	return function() {
 		var context = this, args = arguments;
 		var later = function() {
 			timeout = null;
 			if (!immediate) func.apply(context, args);
 		};
 		var callNow = immediate && !timeout;
 		clearTimeout(timeout);
 		timeout = setTimeout(later, wait);
 		if (callNow) func.apply(context, args);
 	};
 };

 // end additions Bastide
(function (root, factory) {
    if (typeof define === 'function' && define.amd) { define(factory) }
    else if (typeof module === 'object' && module.exports) { module.exports = factory() }
    else { root.Splitpane = factory() }
}(this, function () {
    'use strict'

    var defaults = {  /* Library defaults, change using the `defaults` static method */
        disable: false,
        handle: 2,
        minHeight: 100,
        minWidth:100,
        templates : [
        [ /* horizontal layout: left, handle and right */
        'top:0; bottom:0; left:0; width: {ratio}%; padding-right: {half}px',
        'top:0; bottom:0; left:{ratio}%; margin-left: -{half}px; width: {handle}px; cursor: {cursor}; z-index: 1',
        'top:0; bottom:0; right:0; width: {supplement}%; padding-left: {half}px'],
        [ /* vertical layout: top, handle and bottom */
        'left:0; right:0; top:0; height: {ratio}%; padding-bottom: {half}px',
        'left:0; right:0; top:{ratio}%; width: 100%; margin-top: -{half}px; height: {handle}px; cursor: {cursor}; z-index: 1',
        'left:0; right:0; bottom:0; height: {supplement}%; padding-top: {half}px']],
        cursor: ['col-resize', 'row-resize']}

    /**
     * Create a Splitpane instance.
     *
     * @param {Node} el - Splitpane DOM node
     * @param {Number} minHeight - Smallest pane height (top/bottom)
     * @param {Number} minWidth - Smallest pane height (left/right)
     *
     * #### Note
     * Process splitpanes, calculate width or height of the handle (depending on orientation) and apply the
     * appropriate styles to children
     *
     * #### Example
     * ```js
     * var split = new Splitpane( '#app' )
     * ```
     * @property {Number} orien - Splitpane orientation: 1 = vertical, 0 = horizontal (*defult)
     * @property {Number} min - Splitpane pane's minimum width (horizontal) or height (vertical)
     * @property {ClientRect} container - Splitpane bounding client rectangle
     * @property {NodeList} children - Splitpane children, [0]=top/left pane, [1]=handle, [2]=bottom/right pane
     * @property {String} cursor - Handle mouse cursor value
     * @property {Number} handle - Handle thickness (`width` or `height` depending on orientation)
     * @constructor
     */
    var self = function (el, options) {

        var orien      = el.classList.contains('vertical') ? 1 : 0
        var min        = defaults[['minWidth', 'minHeight'][orien]]
        var container  = el.getBoundingClientRect()
        var children   = el.querySelectorAll(':scope > div')
        var cursor     = window.getComputedStyle(children[1]).getPropertyValue('cursor')
        var handle     = children[1].getBoundingClientRect()[['width', 'height'][orien]]

        /* set defaults for handle thickness and mouse cursor if not set */
        handle = handle < 1 || handle === container[['width', 'height'][orien]] ? defaults.handle : handle
        cursor = cursor === 'auto' ? defaults.cursor[orien] : cursor,

        /* Handle */ children[1].addEventListener('mousedown', function (event) {
            var max = container[['width', 'height'][orien]] - min

            var listeners = [
                function mousemove(event) {
                    var xy = event[['pageX', 'pageY'][orien]] - container[['left', 'top'][orien]]

                    event.preventDefault()
                    _compile(( xy > min ? xy < max ? xy : max : min ) / container[['width', 'height'][orien]])

                    // console.log(event.target,event);
                    getSurfaces()

                },
                function mouseup( event ) {
                    listeners.forEach(function (l) { document.removeEventListener(l.name, l)
                    window.dispatchEvent(new Event('resize')) /* inform everybody */ })
            }]
            listeners.forEach(function (l) { document.addEventListener(l.name, l) })
        })

        /**
         * @function _compile - Updates Splitpane children dimension
         *
         * @param {Number} ratio - The portion (a percentage) that the first child (top/left)
         *                         occupies within the overall width/height of the parent pane
         * @private
         */
        function _compile(ratio) {
            children.forEach(function (child, i) {
                child.style = 'position: absolute; ' + _mustach( defaults.templates[orien][i], {
                    half:  handle / 2,
                    handle: handle,
                    cursor: cursor,
                    ratio: ratio  * 100,
                    supplement: (1 - ratio) * 100
                })
            })
        }

        /**
         * @function _mustach -  Super tiny mustach-like template parser with nested paths support (i.e. x.y.z)
         *
         * @param {String} template - Template string
         * @param {Object} data - Context data
         * @param {strig} path - Nestet path prefix
         * @returns {String}
         * @private
         */
        function _mustach(template, data, path) {
            path = path || ''
            Object.keys(data || {}).forEach(function (key) {
                template = typeof data[key] === 'object' ?
                    _mustach(template, data[key], path + key + '.') :
                    template.replace(new RegExp("{ *?" + path + key + " *?}", 'g'), data[key])
            })
            return template
        }

        /**
         * @function resize -  Public function to adjust splitpane size
         * @public
         */
        this.resize = function(){
            container = el.getBoundingClientRect()
        }

        var attr = ['width', 'height'][orien] /* Calculate ratio (@see _compile) with either *
                                               * the first or the second pane's width/height */
        _compile((children[0].getBoundingClientRect()[attr]  < container[attr] ?
                  children[0].getBoundingClientRect()[attr]  : container[attr] -
                  children[2].getBoundingClientRect()[attr]) / container[attr] )
    }

    /**
     * @function defaults - Override class defaults
     *
     * @param {Object} options - Name value pairs
     * @static
     */
    self.defaults = function (options) {
        Object.keys( options || {} ).forEach( function( key ){ defaults[key] = options[key] })
    }

    /**
     * @function init - Activate all splitpanes, register resize event
     */
     function init() {
        var panes = [].slice.call(document.querySelectorAll('.splitpane') || []).map(
                        function (splitpane) { return new self(splitpane) })

        panes.length > 0 && !defaults.disable && window.addEventListener('resize',
                        function(event){ panes.forEach(function(pane){ pane.resize() })}, true)
     }

    /* Register a document onload event or execute initialize directly otherwise */
    if (document.readyState === "complete") { init() } else {window.addEventListener("load", init)}

    return self
}))
