const d = document
const players = d.getElementById('players')
const video = d.getElementById('video')
const root = d.getElementById("svgs");
let detect = true
let played = false
let contextCreated = false

const synth = new Tone.Synth(
  {
   oscillator : {
     type : 'sine'
   },
   envelope : {
     attack : 0.005 ,
     decay : 0.01 ,
     sustain : 0.003 ,
     release : .4
   }
 }
).toMaster()

// // get a blob SVG element
function init(){
  const blob = generateBlob();
  root.appendChild(blob);
}
nbr = 0
function createPlayer(){
  synth.triggerAttackRelease('C7', '8n')
  const randomBlob = generateBlob();
  root.innerHTML = "<svg></svg>"
  root.appendChild(randomBlob);
  nbr++
}

d.onclick = function(){
  if (!played) {
    if (!contextCreated) {      
      window.context = new AudioContext();
      videoContext = context.createMediaElementSource(video);
      analyser = context.createAnalyser(); //we create an analyser
      analyser.smoothingTimeConstant = 0.9;
      analyser.fftSize = 512; //the total samples are half the fft size.
      videoContext.connect(analyser);
      analyser.connect(context.destination);
      contextCreated = true
    }
    draw();
  
    createPlayer()
    video.play()
    played = true
  }else{
    video.pause()
    played = false
  }
}

function draw() {
  var array = new Uint8Array(analyser.fftSize);
  analyser.getByteTimeDomainData(array);

  var average = 0;
  var max = 0;
  for (i = 0; i < array.length; i++) {
    a = Math.abs(array[i] - 128);
    average += a;
    max = Math.max(max, a);
  }
  average /= array.length;
  if (average > 10 && detect) {
    createPlayer()
    detect = false
    setTimeout(function(){
      detect = true
    }, 60);
  }
  requestAnimationFrame(draw);
}