// Code modified after https://codepen.io/dev_loop/pen/VwwXRpK

console.clear();

class Circle {
	constructor(pos, radius, color, distFromCenter, angle) {
		this.origin = {
			x: pos.x,
			y: pos.y,
		};
		this.distFromCenter = distFromCenter;
		this.radius = radius;
		this.color = color;
		this.angle = angle;
		this.angleIncrement = settings.angleIncrement.central;
		this.endpoint = {
			x: this.origin.x + this.distFromCenter * cos(this.angle),
			y: this.origin.y + this.distFromCenter * sin(this.angle),
		};
		this.posHistory = [];
		this.counter = 0;
		this.tail_length = settings.tail_length;
		this.px = this.endpoint.x;
		this.py = this.endpoint.y;
	}
	draw() {
		this.endpoint.x = this.origin.x + this.distFromCenter * cos(this.angle);
		this.endpoint.y = this.origin.y + this.distFromCenter * sin(this.angle);
		if (settings.showLines) {
			stroke("#000");
			line(this.origin.x, this.origin.y, this.endpoint.x, this.endpoint.y);
			fill(this.color);
			noStroke();
			// ellipse(this.endpoint.x, this.endpoint.y, this.radius);
		}
		this.update();
	}
	update() {
		this.angle += this.angleIncrement /20;
		this.tail_length = settings.tail_length;
	}
	randomDistFromCenter(max) {
		this.distFromCenter = Math.sin(this.angle) * max;
	}
	changePos(fromWhich) {
		this.origin.x = fromWhich.endpoint.x;
		this.origin.y = fromWhich.endpoint.y;
	}
	drawTails() {
		this.px = this.endpoint.x;
		this.py = this.endpoint.y;
		let vector = createVector(this.endpoint.x, this.endpoint.y);
		this.posHistory.push(vector);
		let color = map(this.counter % 255, 0, 255, 0, 255);
		beginShape();
		for (let i = 0; i < this.posHistory.length; i++) {
			let pos = this.posHistory[i];
			noFill();
			if (settings.colorChange) {
				stroke(color, 255, 255, 200);
			} else {
				stroke("#000");
			}
			if (settings.connectLines) {
				line(this.px, this.py, pos.x, pos.y);
			}
			vertex(pos.x, pos.y);
		}
		endShape();
		if (this.posHistory.length > this.tail_length) {
			this.posHistory.splice(0, 1);
		}
		this.counter++;
	}
}

function degToRad(degrees) {
	return degrees * (Math.PI / 180);
}

function returnAngle(index) {
	switch (index) {
		case 0:
			return 0;
		case 1:
			return degToRad(180);
		case 2:
			return degToRad(180 - 90);
		case 3:
			return degToRad(180 + 90);
		case 4:
			return degToRad(180 - 90 / 2);
		case 5:
			return degToRad(90 - 90 / 2);
		case 6:
			return degToRad(180 + 90 / 2);
		case 7:
			return degToRad(360 - 90 / 2);
	}
}

function generateCircle(n, originCircle, props) {
	let circles = [];
	for (let i = 0; i < n; i++) {
		let angle = returnAngle(i);
		let c = new Circle(
			{ x: originCircle.endpoint.x, y: originCircle.endpoint.y },
			props.radius,
			props.color,
			props.distFromCenter,
			angle
		);
		circles.push(c);
	}
	return circles;
}

let cMain;
let circles;

var r = (Math.random() * 0.2) + 0.04;
var settings = {
	angleIncrement: {
		central: -0.05,
		other: - r,
		random() {
			changeAngleIncrement(circles);
		},
	},
	radius: {
		central: 300,
		other: 20,
		random() {
			changeDistFromCenter(circles, this.other + 20);
		},
	},
	totalExtras: 2,
	tail_length: 3920,
	showLines: true,
	connectLines: false,
	colorChange: false,
	followCursor: false,
};

function changeAngleIncrement(arr) {
	arr.forEach(item => {
		item.angleIncrement = (Math.random() - 0.5) * 0.5;
	});
}
function changeDistFromCenter(arr, max) {
	arr.forEach(item => {
		item.posHistory = [];
		item.distFromCenter = Math.random() * max;
	});
}

function setup() {
	createCanvas(innerWidth, innerHeight);
	colorMode(RGB, 255);
	cMain = new Circle(
		{
			x: innerWidth / 2,
			y: innerHeight / 2,
		},
		10, "#fff", settings.radius.central, 0
	);
	circles = generateCircle(settings.totalExtras, cMain, {
		radius: 10,
		color: "red",
		distFromCenter: settings.radius.other,
	});
	circles.forEach(c => {
		c.angleIncrement = settings.angleIncrement.other;
		c.posHistory = [];
	});
}

function draw() {
	background(175,170,170);
	cMain.draw();
	circles.forEach(c => {
		cMain.angleIncrement = settings.angleIncrement.central;
		c.draw();
		c.changePos(cMain);
		c.drawTails();
	});
	if (settings.followCursor) {
		cMain.distFromCenter = 0;
		cMain.origin.x = mouseX;
		cMain.origin.y = mouseY;
	}
	else{
		cMain.distFromCenter = settings.radius.central;
		cMain.origin.x = innerWidth/2;
		cMain.origin.y = innerHeight/2;
	}
}
